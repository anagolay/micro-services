import glob from 'it-glob';
import { Stats } from 'node:fs';
import { stat } from 'node:fs/promises';

/**
 * Walk dir in generator manner
 * @param currentPath -
 * @param pattern -
 * @public
 */
export async function* walk(currentPath: string, pattern: string = '**/*'): AsyncGenerator<Stats> {
  const globOptions = {
    nodir: false,
    realpath: false,
    absolute: true,
    dot: true,
    follow: true
  };

  for await (const p of glob(currentPath, pattern, globOptions)) {
    const fileStat = await stat(p);
    yield fileStat;
  }
}

/**
 * Calculate dir size
 * @param currentPath -
 * @public
 */
export async function dirSize(currentPath: string): Promise<void> {
  let total: number = 0;
  for await (const file of walk(currentPath)) {
    total += file.size;
    console.log(total);
  }

  console.log('total', total);
}
