/* eslint-disable @typescript-eslint/no-explicit-any */
import { Static } from '@sinclair/typebox';
import { Agenda, Job } from 'agenda';
import { mkdir, readFile, rm, writeFile } from 'fs/promises';
import Handlebars from 'handlebars';
import { ObjectId } from 'mongodb';
import path from 'path';
import { performance } from 'perf_hooks';

import { insertOne, IWorkflowVersionDocument } from '../db/workflowCollection';
import { createWasmArtifactsWithIpfsUpload, hostGitRepo, wasmArtifact } from '../utils/artifacts';
import { executeMakers } from '../utils/exec';
import { cloneRepo } from '../utils/git';
import createLogger, { Logger } from '../utils/logger';
import { ISpawnProcessReturn } from '../utils/spawn';
import { readdirRecursive } from '../utils/utils';
import {
  AddWorkflowToQueuePayload,
  IAgendaJob,
  IVersionArtifact,
  Operation,
  OperationVersion,
  OperationVersionReference,
  Segment
} from './types';

// how we call this task
const taskName: string = 'publishWorkflow';

const log: Logger = createLogger({ name: taskName });

/**
 * Configure the Job
 * @param agenda -
 */
export default async function configure(agenda: Agenda): Promise<void> {
  agenda.define(taskName, { priority: 20, concurrency: 1 }, async (job: Job): Promise<any> => {
    try {
      log.info(`Started Workflow publish task ${job.attrs._id}`);
      // Prevent this job from running again
      job.disable().save();

      const { index, payload } = job.attrs.data as IAgendaJob;
      const { manifestData, operations, operationVersions } = payload as Static<
        typeof AddWorkflowToQueuePayload
      >;

      let artifacts: IVersionArtifact[] = [];
      const artifactsPerfStart = performance.now();

      // Prepares the workflow crate from the template files
      const localRepoPath = await prepareWorkflow(
        job.attrs._id,
        log,
        manifestData,
        operations,
        operationVersions
      );

      // Upload generated files as git repository
      const rehosted = await hostGitRepo(localRepoPath);
      artifacts.push(rehosted);

      // let's build the workflow
      const buildOutput = await buildWorkflow(localRepoPath, log);

      const wasmArtifacts = await createWasmArtifactsWithIpfsUpload(
        {
          localRepoPath,
          extraPackageJson: {
            anagolay: {
              type: 'workflow',
              // stringify-ing then parsing resolves as a proper string
              manifest: JSON.parse(JSON.stringify(manifestData))
            }
          }
        },
        log
      );
      artifacts = artifacts.concat(wasmArtifacts);

      // add the WASM artifact
      const wasm = await wasmArtifact(
        {
          localRepoPath
        },
        log
      );
      artifacts.push(wasm);

      const artifactsPerfEnd = (performance.now() - artifactsPerfStart) / 1000;

      // cleanup the mess
      await cleanup([localRepoPath], log);

      const workflowVersionItem: IWorkflowVersionDocument = {
        index,
        jobId: job.attrs._id?.toString() as string,
        manifest: '0x' + Buffer.from(JSON.stringify(manifestData, null, 2)).toString('hex'),
        artifacts: {
          performance: {
            execInSec: artifactsPerfEnd
          },
          items: artifacts
        },
        buildOutput
      };
      await insertOne(workflowVersionItem);

      log.info('end of the task');
    } catch (error) {
      console.error('caught the error in the job', error);
      job.fail(error as string);
    }
  });
}

/**
 * Cleanup the env and containers
 * @param paths -
 * @param log -
 */
async function cleanup(paths: string[], log: Logger): Promise<void> {
  log.info(`Cleaning up the paths`);
  paths.map(async (p) => {
    await rm(p, { recursive: true, force: true });
  });
}

async function buildWorkflow(repositoryLocation: string, log: Logger): Promise<ISpawnProcessReturn> {
  log.info('Building the workflow ...');
  return await executeMakers(['--makefile', 'Makefile.toml', 'workflow-flow'], repositoryLocation, log);
}

/**
 * Prepare Workflow
 * @param id -
 * @param log -
 * @param manifestData -
 * @param operations -
 * @param operationVersions -
 * @returns
 */
async function prepareWorkflow(
  id: ObjectId | undefined,
  log: Logger,
  manifestData: Static<typeof AddWorkflowToQueuePayload.manifestData>,
  operations: Static<typeof AddWorkflowToQueuePayload.operations>,
  operationVersions: Static<typeof AddWorkflowToQueuePayload.operationVersions>
): Promise<string> {
  log.info(`Preparing the workflow from a template`);

  const { repoPath: localRepoPath } = await cloneRepo(
    {
      subfolder: 'anagolay/workflow_publish',
      repository: 'https://gitlab.com/anagolay/anagolay-workflow-template',
      bare: false
      // keepExisting: true // useful to debug workflow template
    },
    log
  );

  // we need clean directory structure, no git commit from the template
  await rm(localRepoPath + '/.git', { recursive: true, force: true });

  // Sort operationsLookup so that the produced code is deterministic
  operations.sort((opl1: Static<typeof Operation>, opl2: Static<typeof Operation>) =>
    opl1.id.localeCompare(opl2.id)
  );

  // Prepare lookup of version according to operation id, for each segment
  const operationsLookup: {
    id: string;
    op: Static<typeof Operation>;
    verId: string;
    ver: Static<typeof OperationVersion>;
    opVerRef: Static<typeof OperationVersionReference>;
  }[][] = manifestData.segments.map(() => []);

  manifestData.segments.forEach((segment: Static<typeof Segment>, index: number) => {
    segment.sequence.forEach((opVerRef) => {
      const version = operationVersions.find(
        (version: Static<typeof OperationVersion>) => opVerRef.versionId === version.id
      );
      const operation = operations.find(
        (operation: Static<typeof Operation>) => version.data.entityId === operation.id
      );

      operationsLookup[index].push({
        id: operation.id,
        op: operation,
        verId: version.id,
        ver: version,
        opVerRef
      });
    });
  });

  // Use std if at least one operation does not support nostd
  const std = operations.find((op: Static<typeof Operation>) => op.data.features.indexOf('std') < 0);

  const destDir = path.normalize(`${localRepoPath}/../${id}`);

  await mkdir(destDir, { recursive: true });

  const workflowFiles = readdirRecursive(localRepoPath);
  let file;
  while (!(file = await workflowFiles.next()).done) {
    const filePath = file.value;
    const source = await readFile(filePath);
    const template = Handlebars.compile(source.toString('utf8'), { noEscape: true });

    const content = template({ version: '0.0.1', manifestData, operations, operationsLookup, std });

    const destSubPath = filePath.substring(localRepoPath.length).replace(/.hbs$/, '');
    const destPath = destDir + destSubPath;
    await mkdir(path.dirname(destPath), { recursive: true });
    await writeFile(destPath, content);
  }

  return destDir;
}
