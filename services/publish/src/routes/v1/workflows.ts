import { FastifyInstance, FastifyPluginOptions, FastifyReply, FastifyRequest } from 'fastify';
import { ObjectId } from 'mongodb';

import { getDB } from '../../db';
import { collectionName } from '../../db/workflowCollection';

/**
 * Home Route for the v1
 * @param fastify -
 * @param options -
 * @param done -
 */
export function listAllWorkflows(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  const opts: FastifyPluginOptions = {
    ...options
  };

  fastify.get('/', opts, async (req: FastifyRequest, reply: FastifyReply) => {
    const db = await getDB();
    const ops = await db.collection(collectionName).find().toArray();

    reply.send(ops);
  });
  done();
}
/**
 * Delete the job
 * @param fastify -
 * @param options -
 * @param done -
 */
export function deleteTheWorkflow(
  fastify: FastifyInstance,
  options: FastifyPluginOptions,
  done: (err?: Error) => void
): void {
  const opts: FastifyPluginOptions = {
    ...options
  };

  fastify.delete<{ Params: { id: string } }>('/:id', opts, async (req, reply) => {
    const { id } = req.params;
    const db = await getDB();
    const rmOp = await db.collection(collectionName).deleteOne({ _id: new ObjectId(id) });
    reply.send(rmOp);
  });
  done();
}
