/**
 * A library for building widgets.
 *
 * @packageDocumentation
 */
import Fastify, { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import oas from 'fastify-oas';
import Handlebars from 'handlebars';
import { Logger } from 'pino';
import { equals, includes, isEmpty, isNil } from 'ramda';

import HomeRouteV1 from './routes/v1/home';
import { deleteTheOperation, listAllOperations } from './routes/v1/operations';
import { checkTheJobId, createAddToQueue, deleteTheJob, listAllJobs } from './routes/v1/queue';
import { deleteTheWorkflow, listAllWorkflows } from './routes/v1/workflows';
import { envs } from './utils/env';
import { isTrue } from './utils/utils';

const { ENABLE_API_KEY_SUPPORT } = process.env;

Handlebars.registerHelper({
  // Transform a string to camel case
  snakeCase: (str = '') => {
    const strArr = str.split(/[\s-]/);
    const snakeArr = strArr.reduce((acc: string, val: string) => {
      return acc.concat(val.toLowerCase());
    }, []);
    return snakeArr.join('_');
  },
  // Helper to get field of object if it exist
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  hasProperty: (object: any, fieldName: string) => object.hasOwnProperty(fieldName),
  // Helper that returns true if the element is contained in the list, false otherwise
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  contains: (list: any[], elem: any) => list.indexOf(elem) > -1,
  // Helper that returns true if the text starts with the prefix, false otherwise
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  startsWith: (text: string, prefix: string) => text.startsWith(prefix),
  // Returns the string concatenation of the arguments
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  concat: (...args: any[]) => {
    const strings = args.slice(0, -1);
    return strings.join('');
  },
  // Helper to sum
  add: (val: number, amount: number) => val + amount,
  // Helper for first order logical conditions
  eq: (v1, v2) => v1 === v2,
  ne: (v1, v2) => v1 !== v2,
  lt: (v1, v2) => v1 < v2,
  gt: (v1, v2) => v1 > v2,
  lte: (v1, v2) => v1 <= v2,
  gte: (v1, v2) => v1 >= v2,
  and() {
    return Array.prototype.every.call(arguments, Boolean);
  },
  or() {
    return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
  }
});

/**
 * Http server instance
 * @public
 * @returns  Modified {@link FastifyInstance}
 */
export default async function createApp(options: { logger?: Logger }): Promise<FastifyInstance> {
  const server = Fastify({
    logger: options.logger
  });

  if (!isNil(ENABLE_API_KEY_SUPPORT) && !isEmpty(ENABLE_API_KEY_SUPPORT) && isTrue(ENABLE_API_KEY_SUPPORT)) {
    console.log('ENABLING THE API-KEY SUPPORT!!!');

    server.addHook('onRequest', async (request, reply) => {
      const skipVerification = ['/', '/heartbeat'];

      if (!includes(request.routerPath)(skipVerification)) {
        const apiKeyFromHeader = request.headers['x-api-key'];
        if (isNil(apiKeyFromHeader) || isEmpty(apiKeyFromHeader)) {
          throw new Error('Did you forget the API key? Set the `x-api-key` header with the correct value.');
        }

        if (!equals(apiKeyFromHeader, envs().APPROVED_API_KEY)) {
          reply.code(401).send(`Your API KEY is not allowed!`);
        }
      }
    });
  } else {
    console.warn('API SUPPORT IS DISABLED, BE CAREFUL NOT TO EXPOSE THIS TO THE PUBLIC!');
  }

  // register docs
  server.register(oas, {
    routePrefix: '/documentation',
    swagger: {
      info: {
        title: 'Publish service',
        description: 'Currently centralized build system for Anagolay Operations and Workflows',
        version: '0.3.0'
      },
      consumes: ['application/json'],
      produces: ['application/json'],
      servers: [
        {
          url: 'http://localhost:2112',
          description: 'Local, Development server'
        }
      ]
    },

    exposeRoute: true
  });

  // register v1 routes
  server.register(HomeRouteV1, { prefix: '/v1' });
  server.register(createAddToQueue, { prefix: '/v1/q' });
  server.register(checkTheJobId, { prefix: '/v1/q' });
  server.register(deleteTheJob, { prefix: '/v1/q' });
  server.register(listAllJobs, { prefix: '/v1/q' });
  server.register(listAllOperations, { prefix: '/v1/operations' });
  server.register(deleteTheOperation, { prefix: '/v1/operations' });
  server.register(listAllWorkflows, { prefix: '/v1/workflows' });
  server.register(deleteTheWorkflow, { prefix: '/v1/workflows' });

  // Declare a home route
  server.get('/', (request: FastifyRequest, reply: FastifyReply): void => {
    reply.send({ message: 'YO, welcome to the Anagolay Publish service, check other API endpoints.' });
  });

  /**
   * Health-check route
   */
  server.get('/healthcheck', (request: FastifyRequest, reply: FastifyReply): void => {
    reply.send({ healthy: true, ts: Date.now() });
  });

  // Run the server!
  await server.listen(envs().APP_PORT, '0.0.0.0');

  return server;
}
