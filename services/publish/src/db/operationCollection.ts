import { Collection, Document } from 'mongodb';

import { IArtifacts } from '../jobs/types';
import { ISpawnProcessReturn } from '../utils/spawn';
import { getDB } from '.';

export const collectionName: string = 'ov_artifacts';

export interface IOperationVersionDocument {
  index: string;
  repository: string;
  jobId: string;
  revision: string;
  manifest: string;
  artifacts: IArtifacts;
  buildOutput: ISpawnProcessReturn;
}

/**
 * Create the version document and return it
 * @param data -
 * @returns
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function insertOne(data: IOperationVersionDocument): Promise<Document> {
  const db = await getDB();
  return await db.collection(collectionName).insertOne(data);
}
/**
 * Return version document collection instance
 * @returns
 */
export async function collection(): Promise<Collection<IOperationVersionDocument>> {
  const db = await getDB();
  return db.collection(collectionName);
}
