/**
 * THIS WHOLE FILE SHOULD BE REMOVED AND THE `@anagolay/utils` USED
 */

import { mkdir, rm, stat } from 'fs/promises';
import { tmpdir } from 'os';
import { isEmpty, isNil, trim } from 'ramda';
import simpleGit from 'simple-git';
import { URL } from 'url';

import exec from './exec';
import createLogger, { Logger } from './logger';

const log: Logger = createLogger({ name: 'git' });

export interface IGitCloneOptions {
  // this is the local path inside /tmp where to clone the repo
  subfolder?: string;
  // this will be prefixed for the dir creation: string;
  repository: string;
  tag?: string;
  revision?: string;
  branch?: string;
  bare?: boolean;
  keepExisting?: boolean;
}
export interface IGitCloneBareOptions {
  unpack?: boolean;
  revision: string;
  repository: string;
  tag?: string;
  branch?: string;
}

/**
 * Normalize Repository to be in a format `username_repo-name`
 * @param pathName -
 * @returns
 */
export function normalizeUrlPathname(pathName: string): string {
  return pathName.replace(/\//gi, '_').replace('.git', '');
}

/**
 * Clone bare repo and return path and by default use the unpacking of the objects
 * @param options -
 * @returns
 */
export async function gitCloneBare(options: IGitCloneBareOptions): Promise<string> {
  const { repository, unpack = true, branch, revision, tag } = options;

  const tmp = tmpdir();
  const normalizedName = `${normalizeUrlPathname(repository)}.git`;
  const repoPath = `${tmp}/anagolay/bare_repos/${normalizedName}`;

  const execOptions = {
    cwd: repoPath,
    shell: '/bin/bash'
  };

  // remove path if exists
  await rm(repoPath, { force: true, recursive: true });

  // for some reason the failed jobs cannot create the empty dir
  await mkdir(`${tmp}/anagolay/rehost-repos`, { recursive: true });

  log.info('Cloning the bare repo', repository, repoPath);
  await exec(`git clone --quiet --bare ${repository} ${repoPath}`, {
    ...execOptions,
    cwd: tmp
  });

  await exec(`git update-server-info`, execOptions);

  if (!isNil(revision)) {
    log.info(`Reseting to the revision ${revision}`);
    /**
     * this is the one  way i figured how to make a commit a HEAD!
     * other one is `git update-ref refs/heads/rehosted 20888c33cd0f6f897703198199f33369cba8639a` but that will not end up in the detached state. maybe that is what we need .... hmmm .....
     */
    await exec(`echo "${revision}" > HEAD`, execOptions);
  } else if (!isNil(tag)) {
    log.info(`Checking out the tag ${tag}`);
    await exec(`git symbolic-ref HEAD refs/tags/${tag.trim()}`, execOptions);
  } else if (!isNil(branch)) {
    log.info(`Checking out the branch ${branch}`);
    await exec(`git symbolic-ref HEAD refs/heads/${branch.trim()}`, execOptions);
  } else {
    throw new Error('No default is set for the rehost!');
  }

  if (unpack) {
    log.info('Unpacking ...');
    await exec(`mv objects/pack/*.pack .`, execOptions);
    // await exec(`cat *.pack | git unpack-objects`, execOptions);
    await exec(`git unpack-objects < *.pack`, execOptions);
    await exec(`rm -rf *.pack`, execOptions);
    await exec(`rm -rf objects/pack/*.idx`, execOptions);
    log.info('Done unpacking.');
  }

  return repoPath;
}

/**
 * Clone repo and return path
 * @param options -
 * @param log -
 */
export async function cloneRepo(
  options: IGitCloneOptions,
  log: Logger
): Promise<{
  repoPath: string;
}> {
  const { subfolder, repository, revision } = options;
  const url = new URL(repository);
  const tmp = tmpdir();
  const normalizedName = `${normalizeUrlPathname(url.pathname)}`;
  const repoPath = `${tmp}/${subfolder}/${normalizedName}${revision ? '-' + revision : ''}`;
  const repoExists = await stat(repoPath).then(
    () => true,
    () => false
  );
  const git = simpleGit();

  // remove path if exists
  if (!options.keepExisting) {
    try {
      await rm(repoPath, { recursive: true, force: true });
    } catch (error) {
      console.log(error);
    }
  } else if (repoExists) {
    log.info(`Keeping the existing repository.`);
    return { repoPath };
  }
  log.info(`Cloning the repo ${url.href}`);

  await git.clone(url.href, repoPath);
  await git.cwd({ path: repoPath, root: true });
  if (!isNil(revision)) {
    await git.checkout(revision);
  }

  log.info(`Repo cloned ${repoPath}`);
  return { repoPath };
}

/**
 * If there are modified or untracked files consider this a very _`dirty`_ repository
 *
 * @remarks This function is using `git status --short`
 * @param path - string
 */
export async function isDirty(path: string): Promise<{
  dirty: boolean;
  changes: string;
}> {
  const cmd = 'git status --short';
  const { stdout } = await exec(cmd, { cwd: path });
  const trimmed = trim(stdout);
  return { dirty: !isEmpty(trimmed), changes: trimmed };
}

/**
 * Use system git to retrieve the HEAD revision
 * @param path - local absolute path to git repo
 * @returns Full length revision hash
 */
export async function headRevision(path: string): Promise<string> {
  const cmd = 'git rev-parse HEAD';
  const { stdout } = await exec(cmd, { cwd: path });
  const trimmed = trim(stdout);
  return trimmed;
}
