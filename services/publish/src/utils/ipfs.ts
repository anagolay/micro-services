/* eslint-disable @typescript-eslint/no-explicit-any */
import { readFile } from 'fs/promises';
import { create, globSource, IPFSHTTPClient, Options } from 'ipfs-http-client';

import { envs } from './env';
import createLogger, { Logger } from './logger';

const log: Logger = createLogger({ name: 'ipfs' });

export let ipfsClient: IPFSHTTPClient;

const { IPFS_GATEWAY_HOSTNAME, IPFS_PIN, IPFS_API_KEY, IPFS_API_URL } = envs();

/**
 * yes! this is intentional, why is it so hard to make a proper software, and keep the types in sync.
 * AddOptions and AddAllOptions are nowhere to be found in normal inclusion path.
 */
type IFuckingIPFS = any;

export const ipfsOptions: IFuckingIPFS = {
  cidVersion: 1,
  wrapWithDirectory: true, // this is important when adding with the httpClient. it behaves differently than the cli where cli will wrap it in the name of the dir, this doesn't do that
  // hashAlg: "blake2b-256",
  // progress: (bytes: number, path?: string) => {
  //   log.info(`${path}`);
  // },
  // fileImportConcurrency: 50,
  pin: IPFS_PIN
};

/**
 * IPFS instance pointing to `@anagolay/ipfs-auth-proxy`
 * @returns
 */
export function createIPFSConnection(): IPFSHTTPClient {
  if (ipfsClient) {
    return ipfsClient;
  }

  const opts: Options = {
    url: IPFS_API_URL,
    headers: {
      'x-api-key': IPFS_API_KEY
    }
  };

  ipfsClient = create(opts);

  return ipfsClient;
}

interface IAddedFiles {
  cid: string;
  path: string;
  size: number;
}

export interface IIpfsResponse extends IAddedFiles {
  url: string;
}

/**
 * Upload the file
 * @param ipfsPath  -
 * @param filePath -
 * @param opts -
 * @returns
 */
export async function uploadViaAdd(ipfsPath: string, filePath: string, opts?: any): Promise<IIpfsResponse> {
  const ipfs: IPFSHTTPClient = createIPFSConnection();

  const ipfsFile = await ipfs.add(
    {
      path: ipfsPath,
      content: await readFile(filePath)
    },
    {
      ...ipfsOptions, // defaults to this to be true, but we don't need it, we want to have direct access to it.
      wrapWithDirectory: false,
      // this makes the leaves to be dag-pg instead of the raw. What is the actual beneft i really don't know
      rawLeaves: false,
      ...opts
    }
  );
  const returnObject = {
    cid: ipfsFile.cid.toString(),
    path: ipfsFile.path,
    size: ipfsFile.size,
    url: `https://${IPFS_GATEWAY_HOSTNAME}/ipfs/${ipfsFile.cid}`
  };
  return returnObject;
}

/**
 * Upload to IPFS via ipfs.addAll, passing options will overwrite the default ones
 * @param path -
 * @param opts -
 */
export async function uploadViaAddAll(path: string, opts?: any): Promise<IIpfsResponse> {
  const ipfs: IPFSHTTPClient = createIPFSConnection();
  log.info('Upload to IPFS started ...');

  const addedFiles: IAddedFiles[] = [];

  for await (const file of ipfs.addAll(globSource(path, '**/*'), {
    ...ipfsOptions,
    ...opts
  })) {
    addedFiles.push({
      cid: file.cid.toString(),
      path: file.path,
      size: file.size
    });
  }

  const lastCid = addedFiles[addedFiles.length - 1];

  if (lastCid && lastCid.path !== '') {
    log.error(`Last Cid is not the root: ${lastCid.path}`);
    throw new Error(`Last Cid is not the root: ${lastCid.path}`);
  }

  const returnObject = {
    cid: lastCid.cid.toString(),
    path: lastCid.path,
    size: lastCid.size,
    url: `https://${IPFS_GATEWAY_HOSTNAME}/ipfs/${lastCid.cid}`
  };

  return returnObject;
}
