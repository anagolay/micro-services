import { exec as originalExec, ExecOptions as BaseExecOptions, PromiseWithChild } from 'child_process';
import { promisify } from 'util';

import { Logger as PinoLogger } from './logger';
import asyncSpawn, { ISpawnProcessReturn } from './spawn';

export interface IExecOptions extends BaseExecOptions {
  encoding?: BufferEncoding;
}

export const defaultExecOptions: IExecOptions = {
  cwd: '.',
  encoding: 'utf8'
};

/**
 * Async Exec with decent defaults
 * @param command - A command you would put in the `exec`
 * @param options - Any of the {@link IExecOptions} options
 * @returns
 */
export default async function exec(
  command: string,
  options: IExecOptions = defaultExecOptions
): Promise<
  PromiseWithChild<{
    stdout: string;
    stderr: string;
  }>
> {
  const execAsync = promisify(originalExec);
  return execAsync(command, options);
}

/**
 * Wraps the common execution for the makers ( cargo make ) and adding common arguments like `--profile production` and `--disable-check-for-updates`.
 * @param commands -
 * @param cwd -
 * @returns
 */
export async function executeMakers(
  commands: string[],
  cwd: string,
  log: PinoLogger
): Promise<ISpawnProcessReturn> {
  return await asyncSpawn(
    'makers',
    ['--disable-check-for-updates', '--profile', 'production', ...commands],
    {
      cwd
    },
    true,
    log
  );
}
