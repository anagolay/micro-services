import pino, { Logger as PinoLogger, LoggerOptions } from 'pino';

export type Logger = PinoLogger;
/**
 * Create the PINO logger with pino pretty if in DEV
 * @param options -
 * @returns
 */
export default function createLogger(options?: LoggerOptions): Logger {
  const logger = pino({
    // transport: {
    //   target: 'pino-pretty',
    //   options: {
    //     colorize: true
    //   }
    // },
    ...options
  });
  return logger;
}
