import { FastifyInstance as origInstance } from 'fastify';
declare module 'fastify' {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  export interface FastifyInstance extends origInstance {
    io: Server;
  }
}
