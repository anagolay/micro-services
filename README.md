# Anagolay Services

## Publish Service

The job of this service is to build, package, rehost and upload the Operation Version and Workflow generated code.

## Websocket Service

The Socket.io based WS mainly used to pass the data from UI to CLI

## License

If not defined per project, LICENSE file is applied.

## Development flow

We are using [Rushstack](https://rushstack.io/) and [Pnpm](https://pnpm.io/). Make sure you have them installed. When you start with the project you will get the access to [doppler](https://doppler.com), if somebody forgets ask [woss](@woss:matrix.org)

Standard `rush` commands apply.

# every time you recreate the containers run this if you want to remove the bootstrap nodes

```sh
docker-compose exec ipfs ipfs bootstrap rm all \
&& docker-compose stop ipfs \
&& docker-compose up -d ipfs
```

