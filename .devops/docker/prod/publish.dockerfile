FROM docker.io/anagolay/base-rust:1-stable-x86_64-unknown-linux-gnu-bullseye as base

# this is the v6.30.0
ADD https://ipfs.anagolay.network/ipfs/QmeCUX9cK4YKdTbNVq3jg5cJPvz8uQiQmb4AKKd7niy4kY /usr/local/bin/pnpm

RUN chmod +x /usr/local/bin/pnpm \
	&& pnpm env use --global 16

RUN cargo install wasm-bindgen-cli --version 0.2.81

# smoke test
RUN rustup --version \
	&& rustc --version \
	&& cargo --version \ 
	&& git --version \
	&& tar --version \
	&& pnpm --version \
	&& node --version \
	&& makers --version \
	&& wasm-pack --version \
	&& wasm-bindgen --version

WORKDIR /build

COPY . .

RUN node common/scripts/install-run-rush.js install \
	&& node common/scripts/install-run-rush.js update \
	&& node common/scripts/install-run-rush.js rebuild --verbose


# MAIN IMAGE FOR THE UPLOAD
FROM docker.io/anagolay/base-rust:1-stable-x86_64-unknown-linux-gnu-bullseye
LABEL maintainer="daniel@woss.io" 
LABEL description="Production ready Publish microservice"
LABEL "network.anagolay.vendor"="Anagolay Network"

ENV PORT=3000

# this is the v6.30.0
COPY --from=base /usr/local/bin/pnpm /usr/local/bin/pnpm

RUN chmod +x /usr/local/bin/pnpm \
	&& pnpm env use --global 16

WORKDIR /app

COPY --from=base /build/services/publish/lib /app
COPY --from=base /build/services/publish/package.json /app
COPY --from=base /build/services/publish/LICENSE /app

RUN pnpm install --no-lockfile --silent --prod

EXPOSE ${PORT}

HEALTHCHECK --interval=30s --timeout=7s CMD curl -f http://127.0.0.1:$PORT/healthcheck || exit 1

CMD [ "node","start.js" ]
